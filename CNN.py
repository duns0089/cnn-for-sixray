# python CNN.py
# tensorboard --logdir=logs/


import random
import os
import shutil
import math
import numpy as np
import time

import tensorflow as tf

from keras.preprocessing.image import ImageDataGenerator
from keras.models import Sequential, load_model
from keras.layers import Conv2D, MaxPooling2D
from keras.layers import Activation, Dropout, Flatten, Dense
from keras import backend as K
from keras.callbacks import TensorBoard

import xml.etree.ElementTree as ET

from sklearn.metrics import classification_report


# GPU configurations
os.environ["TF_GPU_ALLOCATOR"] = "cuda_malloc_async"

images_path = "./t_images_00_p/"
# images_path = "./t_images_00_01_p/"

data_path = "./data/"
annotations_path = "./Annotation/"

train_path = "./data/train/"
test_path = "./data/test/"

train_negative = "./data/train/negative/"
train_positive = "./data/train/positive/"

train_hammer = "./data/train/hammer/"
train_wrench = "./data/train/wrench/"
train_gun = "./data/train/gun/"
train_knife = "./data/train/knife/"
train_pliers = "./data/train/pliers/"

test_negative = "./data/test/negative/"
test_positive = "./data/test/positive/"

test_hammer = "./data/test/hammer/"
test_wrench = "./data/test/wrench/"
test_gun = "./data/test/gun/"
test_knife = "./data/test/knife/"
test_pliers = "./data/test/pliers/"


train_portion_frac = 0.90 # 70% for train, 20% for validation
test_portion_frac = 1 - train_portion_frac # 10% for evaluation



def MakeDir(path):
    if not os.path.isdir(path):
        os.mkdir(path)

# Given a positive image file, return all the threats in the image 
def GetThreats(file):
    threats = []
    annotation_file = file[:6] + ".xml"
    annotations_file_path = os.path.join(annotations_path, annotation_file)

    if os.path.isfile(annotations_file_path):
        tree = ET.parse(annotations_file_path)
        root = tree.getroot()

        for i in root:
            if i.tag == "object":
                threat = i.find("name")
                if threat is not None:
                    threats.append(threat.text)
                else:
                    print(file + " has bad annotations file")
    else:
        print("could not find: " + annotations_file_path)

    return threats

# Given a positive image file, move the image into the correct threat folder(s)    
def MoveToThreatDir(file, train_or_validation):
    file_path = os.path.join(images_path, file)
    threats = GetThreats(file)

    for threat in threats:
        lower_threat = threat.lower()

        if lower_threat == "wrench":
            MakeDir(os.path.join(data_path, train_or_validation, "wrench"))
            new_path = os.path.join(data_path, train_or_validation, "wrench", file)
            # print(file_path + " -> " + new_path)
            shutil.copy2(file_path, new_path)

        if lower_threat == "gun":
            MakeDir(os.path.join(data_path, train_or_validation, "gun"))
            new_path = os.path.join(data_path, train_or_validation, "gun", file)
            # print(file_path + " -> " + new_path)
            shutil.copy2(file_path, new_path)
    
        if lower_threat == "knife":
            MakeDir(os.path.join(data_path, train_or_validation, "knife"))
            new_path = os.path.join(data_path, train_or_validation, "knife", file)
            # print(file_path + " -> " + new_path)
            shutil.copy2(file_path, new_path)

        if lower_threat == "pliers":
            MakeDir(os.path.join(data_path, train_or_validation, "pliers"))
            new_path = os.path.join(data_path, train_or_validation, "pliers", file)
            # print(file_path + " -> " + new_path)
            shutil.copy2(file_path, new_path)

        if lower_threat == "scissors":
            MakeDir(os.path.join(data_path, train_or_validation, "scissors"))
            new_path = os.path.join(data_path, train_or_validation, "scissors", file)
            # print(file_path + " -> " + new_path)
            shutil.copy2(file_path, new_path)

    os.remove(file_path)


def main():
    ###
    ### Prepare the data, split the data into train/test and seperate threats from negative images
    ###
    print("Making random file array ...")
    files = []
    for file in os.listdir(images_path):
        files.append(file)
    random.shuffle(files)
    print("... done. Array size: " + str(len(files)) + " (number of files)\n")

    print("Making data directories ...")
    shutil.rmtree(data_path, ignore_errors=True)
    MakeDir(data_path)
    MakeDir(train_path)
    MakeDir(test_path)
    MakeDir(test_negative)
    MakeDir(train_negative)
    print("... directories made\n")

    train_total = math.ceil(len(files)*train_portion_frac)
    test_total = len(files) - train_total

    print("Moving train photos: " + str(train_total) + " of them ...")
    for file in files[0:train_total]:
        file_path = os.path.join(images_path, file)
        train_negative_file_path = os.path.join(train_negative, file)

        if file[0] == "N":
            # print(file_path + " -> " + train_negative_file_path)
            os.rename(file_path, train_negative_file_path)
        else:
            MoveToThreatDir(file, "train")
        
    print("... Train files done\n")

    print("Moving test photos: " + str(test_total) + " of them ...")
    for file in files[-test_total:]:
        file_path = os.path.join(images_path, file)
        test_negative_file_path = os.path.join(test_negative, file)

        if file[0] == "N":
            # print(file_path + " -> " + test_negative_file_path)
            os.rename(file_path, test_negative_file_path)
        else:
            MoveToThreatDir(file, "test")

    print("... Test files done\n")
    os.rmdir(images_path)


    ###
    ### Get count of train and validation files
    ###
    sum_of_train_imgs = sum([len(files) for r, d, files in os.walk(train_path)])
    sum_of_validation_imgs = sum([len(files) for r, d, files in os.walk(test_path)])

    print("Training images:", sum_of_train_imgs)
    print("Test images:", sum_of_validation_imgs)
    print("Some positive images are trained twice when marked with multiple objects\n")

    ###
    ### Tensorboard setup
    ###
    tensor_board_name = "CNN-{}".format(int(time.time()))
    tensorboard = TensorBoard(log_dir="logs/{}".format(tensor_board_name))

    ###
    ### Hyper Parameters
    ###
    img_width, img_height = 500,500
    train_data_dir = train_path
    validation_data_dir = test_path
    epochs = 1
    batch_size = 15

    if K.image_data_format() == 'channels_first':
        input_shape = (1, img_width, img_height)
    else:
        input_shape = (img_width, img_height, 1)


    ###
    ### Define the CNN Model sequentially
    ###
    model = Sequential()
    model.add(Conv2D(32, (3, 3), input_shape=input_shape))
    model.add(Activation('relu'))
    model.add(MaxPooling2D(pool_size=(2, 2)))

    model.add(Conv2D(64, (3, 3)))
    model.add(Activation('relu'))
    model.add(MaxPooling2D(pool_size=(2, 2)))

    model.add(Conv2D(64, (3, 3)))
    model.add(Activation('relu'))
    model.add(MaxPooling2D(pool_size=(2, 2)))

    model.add(Conv2D(128, (3, 3)))
    model.add(Activation('relu'))
    model.add(MaxPooling2D(pool_size=(2, 2)))

    model.add(Flatten())
    model.add(Dense(64))
    model.add(Activation('relu'))

    model.add(Dropout(0.5))
    model.add(Dense(6)) # make same as number of classses in data
    model.add(Activation('softmax'))

    ###
    ### Compile the model
    ###
    model.compile(loss='categorical_crossentropy', # https://keras.io/api/losses/
                optimizer='adam', # https://keras.io/api/optimizers/
                metrics=['accuracy']) 

    ###
    ### Create the Image Data Generators that flow from direcotry
    ###
    # train_datagen is split into a train and validations set (validation != test)
    train_datagen = ImageDataGenerator(
        rescale=1. / 255,
        shear_range=0.2, # objects may be oriented differently
        zoom_range=0.2, # to adhere to different position threats
        horizontal_flip=True,
        validation_split=2/9)

    # this set is for evaluating the model afterwards
    test_datagen = ImageDataGenerator(rescale=1. / 255)

    train_generator = train_datagen.flow_from_directory(
        train_data_dir,
        target_size=(img_width, img_height),
        batch_size=batch_size,
        color_mode='grayscale',
        subset="training",
        class_mode='categorical')

    validation_generator = train_datagen.flow_from_directory(
        train_data_dir,
        target_size=(img_width, img_height),
        batch_size=batch_size,
        color_mode='grayscale',
        subset="validation",
        class_mode='categorical')

    # test uses a seperate folder than train and validation
    test_generator = test_datagen.flow_from_directory(
        validation_data_dir,
        target_size=(img_width, img_height),
        batch_size=batch_size,
        color_mode='grayscale',
        class_mode='categorical')

    ###
    ### Fit the model, using HyperParameters
    ###
    nb_train_samples = len(train_generator.classes)
    nb_validation_samples = len(validation_generator.classes)

    model.fit(
        train_generator,
        steps_per_epoch=nb_train_samples // batch_size,
        epochs=epochs,
        validation_data=validation_generator,
        validation_steps=nb_validation_samples // batch_size,
        callbacks=[tensorboard]
    )

    ###
    ### Evaluate the model with the test set
    ###
    # Predict Classes
    predictions = model.predict(test_generator, verbose=1)

    predicted_classes = np.argmax(predictions, axis=-1)

    true_classes = test_generator.classes
    class_labels = list(test_generator.class_indices.keys())

    print("predicted_class = " + str(predicted_classes))
    print("size = " + str(len(predicted_classes)))

    print("true_class = " + str(true_classes))
    print("size = " + str(len(true_classes)))

    print("class_labels = " + str(class_labels))
    print("size = " + str(len(class_labels)))

    # Print the model
    cnn_classification_report = classification_report(true_classes, predicted_classes, target_names = class_labels)
    print(cnn_classification_report)

    ###
    ### Finished, save the model
    ###
    model_name = "CNN-{}".format(int(time.time()))
    model.save(model_name + ".model")

### MAIN ####
if __name__ == "__main__":
    main()
